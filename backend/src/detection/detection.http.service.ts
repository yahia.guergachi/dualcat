import { Controller, HttpService, Injectable, Post } from '@nestjs/common';
const axios = require('axios');
const FormData = require('form-data');
var request = require('request');
const fs = require('fs');
const Path = require('path')


@Injectable()
export class DetectionService {
    THRESHOLD = 400;

    constructor() {

    }


    async detectCats(index) {
        //Pour faciliter la reconnaissance, on met un seuil minimal pour que le chat ne soit pas trop rogné.
        const i = index + this.THRESHOLD;
        console.log("detecting https://placekitten.com/400/" + i)
        var url = 'https://placekitten.com/400/' + i;
        try {
            var image = await axios(
                {
                    url,
                    method: 'GET',
                    responseType: 'stream'
                }
            );
            let formData = new FormData();
            formData.append('model', 'yolov4');
            formData.append('image', image.data);
            try {
                console.log('sending detection request')

                var result = await axios({
                    method: "post",
                    url: "http://130.211.52.221/api/v1/detection",
                    data: formData,
                    headers: { 'Content-Type': 'multipart/form-data;' },
                })
                //fs.writeFileSync(__dirname + "../../../static", 'kitten' + i + '.jpg', image)
                result.data.url = url;
                console.log('returning result for', url)
                return { url: url, score: result.data.predictions[0].score, label: result.data.predictions[0].label }
            } catch (error) {
                console.log(error)
                return { url: url, predictions: [{ score: 0, label: null }] }
            }
        } catch (error) {
            console.log(error)
        }

        return {};


    }


}