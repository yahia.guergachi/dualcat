import { Controller, Get, Post, Req, Res } from '@nestjs/common';
import { DetectionService } from './detection.http.service';
const axios = require('axios');

@Controller()
export class DetectionController {
    constructor(private detectionService: DetectionService) {

    }

    @Post('/detect')
    async signIn(@Req() request, @Res() response) {
        try {
            let body = request.body;
            console.log(body, Number.parseInt(body.count))
            var stats = await this.detectionService.detectCats( Number.parseInt(body.count));
            console.log('finish detection ',body.count)
            return response.status(200).send(stats);
        } catch {
            return response.status(500).send("Erreur serveur");
        }
    }
}
