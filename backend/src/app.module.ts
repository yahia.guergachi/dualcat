import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DetectionController } from './detection/detection.controller';
import { DetectionService } from './detection/detection.http.service';

@Module({
  imports: [],
  controllers: [AppController, DetectionController],
  providers: [AppService, DetectionService],
})
export class AppModule { }
