import { DetectionService } from './detection.http.service';
export declare class DetectionController {
    private detectionService;
    constructor(detectionService: DetectionService);
    signIn(request: any, response: any): Promise<any>;
}
