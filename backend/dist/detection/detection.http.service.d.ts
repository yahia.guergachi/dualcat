export declare class DetectionService {
    THRESHOLD: number;
    constructor();
    detectCats(index: any): Promise<{
        url: string;
        score: any;
        label: any;
        predictions?: undefined;
    } | {
        url: string;
        predictions: {
            score: number;
            label: any;
        }[];
        score?: undefined;
        label?: undefined;
    } | {
        url?: undefined;
        score?: undefined;
        label?: undefined;
        predictions?: undefined;
    }>;
}
