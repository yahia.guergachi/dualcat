"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetectionService = void 0;
const common_1 = require("@nestjs/common");
const axios = require('axios');
const FormData = require('form-data');
var request = require('request');
const fs = require('fs');
const Path = require('path');
let DetectionService = class DetectionService {
    constructor() {
        this.THRESHOLD = 400;
    }
    async detectCats(index) {
        const i = index + this.THRESHOLD;
        console.log("detecting https://placekitten.com/400/" + i);
        var url = 'https://placekitten.com/400/' + i;
        try {
            var image = await axios({
                url,
                method: 'GET',
                responseType: 'stream'
            });
            let formData = new FormData();
            formData.append('model', 'yolov4');
            formData.append('image', image.data);
            try {
                console.log('sending detection request');
                var result = await axios({
                    method: "post",
                    url: "http://130.211.52.221/api/v1/detection",
                    data: formData,
                    headers: { 'Content-Type': 'multipart/form-data;' },
                });
                result.data.url = url;
                console.log('returning result for', url);
                return { url: url, score: result.data.predictions[0].score, label: result.data.predictions[0].label };
            }
            catch (error) {
                console.log(error);
                return { url: url, predictions: [{ score: 0, label: null }] };
            }
        }
        catch (error) {
            console.log(error);
        }
        return {};
    }
};
DetectionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], DetectionService);
exports.DetectionService = DetectionService;
//# sourceMappingURL=detection.http.service.js.map