"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetectionController = void 0;
const common_1 = require("@nestjs/common");
const detection_http_service_1 = require("./detection.http.service");
const axios = require('axios');
let DetectionController = class DetectionController {
    constructor(detectionService) {
        this.detectionService = detectionService;
    }
    async signIn(request, response) {
        try {
            let body = request.body;
            console.log(body, Number.parseInt(body.count));
            var stats = await this.detectionService.detectCats(Number.parseInt(body.count));
            console.log('finish detection ', body.count);
            return response.status(200).send(stats);
        }
        catch (_a) {
            return response.status(500).send("Erreur serveur");
        }
    }
};
__decorate([
    (0, common_1.Post)('/detect'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DetectionController.prototype, "signIn", null);
DetectionController = __decorate([
    (0, common_1.Controller)(),
    __metadata("design:paramtypes", [detection_http_service_1.DetectionService])
], DetectionController);
exports.DetectionController = DetectionController;
//# sourceMappingURL=detection.controller.js.map